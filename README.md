To run the project in local environment:
----------------------------------------

- clone the project in your machine.
- inside de folder apinodereactredis_3_containers, after intall docker run in a terminal: docker-compose up --build .
- create new user credentials using POSTMAN calling to http://localhost:5000/user/add using the post http verb, send the fallowing json :

    {
        "username":"here put the value to identify the new user",
        "password":"and the pass"
    }

    if everithing go ok, you recive a response from the node server with this message:

    {
        "message": "ok user created"
    }

- login to the app in this direcction http://localhost:8080/, and using the credentials created in the previous point.
- with correct credentials, the system create a new session (2 minutes) to use the app, after the correct singIn you will be redirected to a list of the charecter from the rick and morty show.
- if you want to run a test using jest, open a terminal inside the project, go to the node-server folder  and run :
    -npm i
    -comment this line in the file: node-server/database.js  -> host: 'redis-server'
    -open a new redis instance inyour machine (redis-serve)
    -run the command: npm run test-dev, to run the test
    -run the command: npm run coverage, to check the % of code coverage realize by the test 





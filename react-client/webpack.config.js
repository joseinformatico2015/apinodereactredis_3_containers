const path=require('path');
const HtmlWebpackPlugin=require('html-webpack-plugin');
const {CleanWebpackPlugin}=require('clean-webpack-plugin');

module.exports={
    entry:["@babel/polyfill", path.resolve(__dirname,"src","index.js")],
    output:{
        path: path.resolve(__dirname,"dist"),
        filename:"bundle.js"
    },
    mode:'development',
    resolve:{
        extensions: ['.js','.jsx']
    },
    module:{
        rules:[
            {
                test: /\.(js|jsx)$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            }
        ]
    },
    devServer:{
            contentBase: path.join(__dirname, 'dist'),
            historyApiFallback: true
    },
    plugins:[
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
    ]
}
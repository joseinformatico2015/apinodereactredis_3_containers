import React, { useState, useEffect } from 'react'
import e from 'cors';
import {Card} from "react-bootstrap";

const axios = require('axios');

const Data=(props)=>{
    const [data, setData] = useState(null);
    const [message, setMessage] = useState("loading data...");
    const [error, setError] = useState(null);

    const getData = async (token) => {
        var config = {
            headers: { 'access-token': token,'Content-Type': 'application/json' }
        };
        const data = await axios.get('http://localhost:5000/data',config)
        // const data = await axios.get('https://rickandmortyapi.com/api/character');
         console.log(data)
        // names, status, species, gender and image.
        // session expire
        if(data.data.mensaje=="Session expired, please login again"){
            setError("Session expired, please login again");
        }else{
            //  var finalData=data.data.results.map((character)=>{
            var finalData=data.data.map((character)=>{
                return {
                    id:character.id,
                    name:character.name,
                    status:character.status,
                    specie:character.species,
                    gender:character.gender,
                    image:character.image
                }
            })
            console.log(finalData.length);
            return finalData;
        }
    };
    
    const renderCard=(card, index)=>{
        return(
            <Card style={{ width: '10rem' }} key={index} className="box">
                <Card.Img variant="top" src={card.image} />
                <Card.Body style={{ padding: '.5rem'}}>
                    <Card.Title style={{ fontSize: '80%',marginTop:".75rem"}}>Name: {card.name}</Card.Title>
                    <Card.Text style={{ fontSize: '80%',marginBottom:".75rem"}}>Status: {card.status}</Card.Text>
                    <Card.Text style={{ fontSize: '80%',marginBottom:".75rem"}}>Specie: {card.specie}</Card.Text>
                    <Card.Text style={{ fontSize: '80%',marginBottom:".75rem"}}>ender: {card.gender}</Card.Text>
                </Card.Body>
            </Card>
        );
    }

    useEffect(() => {
        // console.log(props.location.state)
        let token="";
        const token_storage=JSON.parse(localStorage.getItem('token'));
        // console.log(token);
        props.location.state?token=props.location.state.token:token=token_storage
        // console.log(token)

        async function fetchData() {
            const response =  await getData(token);
            // console.log("respuesta fetchData "+JSON.stringify(response))
            // setData(response);
            return response
        }
        fetchData().then((data)=>{setData(data)})
        
    },[]);

    return (
        error?<div>{error}</div>:
        //  data!=null?<div className="grid">{data.map(renderCard)}</div>:<div>{message}</div>
        data!=null?data.map((elem)=><div key={elem.id}>{elem.id}. <span  style={{ fontSize: '120%',color:"red"}}>{elem.name}</span>, Status: {elem.status}, Gender: {elem.gender}, Specie: {elem.specie}, Image: <a href={elem.image}>{elem.image}</a></div>):<div>{message}</div>
    )
}

export default Data

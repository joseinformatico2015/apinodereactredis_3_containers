import React, { Component } from 'react'
const axios = require('axios');
import { Redirect } from 'react-router'

class SignIn extends Component{
    state={
        username:'',
        password:'',
        auth:'',
        token:'',
        wrongCredentials:false
    }

    async makeRequest() {
        var config = {
            headers: { 'access-token': 'Console app','Content-Type': 'application/json' }
          };
        
        let res = await  axios.post('http://localhost:5000/login', this.state, config)
        console.log("in SigIN component");
        // console.log(res);
        if(res.data.token){
            // console.log(JSON.stringify(res.data.token);
            localStorage.setItem('token', JSON.stringify(res.data.token));
           this.setState({auth:true,token:res.data.token});
        }else{
            this.setState({wrongCredentials:true})
        }
    }

    handleChange=(e)=>{
        this.setState({
            [e.target.id]:e.target.value

        })
        console.log(this.state);
    }    
    handleSubmit=(e)=>{
        e.preventDefault();
        
        console.log("enviando");
        this.makeRequest();
    }
    render(){
        return (
            this.state.auth?<Redirect to={{pathname:"/data", state:{token:this.state.token}}} /> :
            <div className="container flex"> 
                <h1 className="mtb-30">Is that you, BirdPerson?</h1>
                <p className="subtitle">Enter your credentials</p>
                <div className="loginBox">
                    <form className="" onSubmit={this.handleSubmit}>

                        <div className="form-flex">
                            <label htmlFor="email" className="subtitle">Username</label>
                            <input type="email" id="username" onChange={this.handleChange}/>
                        </div>
                        <div className={ this.state.wrongCredentials ? 'form-flex-error' : 'form-flex' }>
                            <label htmlFor="password" className="subtitle">Password</label>
                            <input type="password" id="password" onChange={this.handleChange}/>
                        </div>
                        <div className={ this.state.wrongCredentials ? 'error' : 'errorHidden' }>Wrong credentials.</div>
                        <div className="form-button">
                            <button className="btn btn-primary" onClick={this.handleSubmit}>Login</button>
                            <div className="">
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
            )
    }
}

export default SignIn

import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import SignIn from './SignIn'
import Data from './Data'

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path='/' component={SignIn}/>
                    <Route exact path='/data'  render={(props)=> <Data {...props} />}></Route>
                </Switch>
            </BrowserRouter>
        )
    }
}
export default App
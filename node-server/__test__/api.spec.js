const supertest = require('supertest');
const app = require('../src/server');
const request = supertest(app)
const jwt = require('jsonwebtoken');
const config = require('../configs/config');

const payload = {
    check:  true
};
const token = jwt.sign(payload, config.llave, {
    expiresIn:  120
});

const testUser={
    username:'testApiUser',
    password: "1234"
}

const testToken="1234567testToken";

// beforeAll(() => {
//     mongoDB.connect();
// });

// afterAll((done) => {
//     mongoDB.disconnect(done);
// });


describe('check the mitical routes of the mitical App', ()=>{

    it('Gets the test endpoint', async done => {
        // Sends GET Request to /test endpoint
        const res = await request.get('/')
        .expect(200)
        .expect('Content-Type',/json/)
        .then((responce)=>{
            console.log(responce.body)
        })
        done()
    })

    it('POST /user/add', async(done) => {
        const data = {
        "username": "testApiUser",
        "password": "1234"
        }
        const res = await request.post("/user/add")
            .send(data)
            .expect(200)
            .then((responce)=>{
                console.log(responce.body)
                expect(responce.body.message).toBe("ok user created")
                done()
            })
    });

    it('POST /login', async done => {
        const data = testUser
        const res = await request.post("/login")
            .send(data)
            .expect(200)
            .then((responce)=>{
                console.log(responce.body)
                // expect(responce.body.message).toBe("ok user created")
            })
            done()
    })


    // it('POST /login', async done => {
    test('POST /login', async done => {
        const data = testUser
        const res = await request.post("/login")
            .send(data)
            .expect(200)
            .then((responce)=>{
                console.log(responce.body)
                // expect(responce.body.message).toBe("ok user created")
            })
            done()
    })

    // test('GET /data ', async done => {
    //     const data = testUser
    //     const res = await request.get("/data")
    //         .set('access-token', testToken) 
    //         .expect(200)
    //         // .then((responce)=>{
    //         //     console.log(responce)
    //         //     // expect(responce.body.message).toBe("ok user created")
    //         // })
    //         done()
    // },30000);

    test('GET /data with bad token', async done =>{
        const data = testUser
        const res = await request.get("/data")
            .set('access-token',"badToken") 
            .expect(200)
            .then((response)=>{
                expect(response.body.mensaje).toBe('Session expired, please login again')
            })
            done()
    });

    test('GET /data with correct token', async done =>{
        const data = testUser
        const res = await request.get("/data")
            .set('access-token',token) 
            .expect(200)
            done()
    },30000);

});

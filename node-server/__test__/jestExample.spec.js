const jestExample = require('../src/controllers/jest.example').jestExample;

describe('jest example test', ()=>{
    it('should return true',()=>{
        //Testing a boolean
        expect(jestExample()).toBeTruthy();
        //Another way to test a boolean
        expect(jestExample()).toEqual(true);
    });

    it('Async test', async done => {
        // Do your async tests here
        
        done()
    })
})
var express = require('express');
var router = express.Router();

const authMiddleware =  require('./middlewares/authMiddleware').authMiddleware;
const publicController = require('../controllers/public.controller')
const privateController = require('../controllers/private.controller')


router.get('/',publicController.hi);
router.post('/user/add',publicController.createUser)
router.post('/login',publicController.findUser)
router.get('/data',authMiddleware, privateController.getData);

module.exports= router

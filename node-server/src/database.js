// REDIS DATABASE

const redis = require('redis');

const client = redis.createClient({
    host: 'redis-server',
    port: 6379
})

client.on('connect', ()=>{
    console.log('Redis is connected');
})

client.on('error', function(err) {
    console.log('Redis error: ' + err);
});

module.exports=client
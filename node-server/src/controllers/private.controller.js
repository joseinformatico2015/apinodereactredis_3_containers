var express = require('express');
const config = require('../../configs/config');
const jwt = require('jsonwebtoken');
const fetch = require("node-fetch");

class privateController{
    static async getData(req,res){
        console.log("en el getData");
        let apiRes = await fetch("https://rickandmortyapi.com/api/character/");
        let data = await apiRes.json();
        
        const allPages = await Promise.all(
            Array(data.info.pages - 1)
            .fill(0)
            .map((i,index) =>
                fetch(`https://rickandmortyapi.com/api/character/?page=${index + 2}`).then(res => res.json()).then(d => d.results)
            )
        )
        const fullData = allPages.reduce((acc, d) => [...acc, ...d], []);
        // console.log([...data.results, ...fullData]);
        res.status(200).json([...data.results, ...fullData]);
        // res.json([...data.results, ...fullData]);//perfect


        // return [...data.results, ...fullData];
    }
}

module.exports = privateController;
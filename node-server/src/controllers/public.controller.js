var express = require('express');
const config = require('../../configs/config');
const client=require ('../database')
const jwt = require('jsonwebtoken');

class publicController{
    static async createUser(req , res) {
        console.log(req.body);
        let username=req.body.username;
        let password=req.body.password;
        let create_at=Date.now();
        client.hmset(username,["password", password,"create_at", create_at],function(err,resp){
            if(err){
                console.log(err);
                res.json({ message: "error" });
            }else{
                console.log("guardado");
                // res.json({ message: "ok user created" });
                res.status(200).json({ message: "ok user created" });
            }
        })
    }
    static async hi(req, res) {
        console.log("hi")
        res.json({ mensaje: "hi"})
    }
    static async findUser(req, res){
        console.log("en controller")
        let username=req.body.username;
        let password=req.body.password;
        client.hgetall(username,function(err,resp){
            if(err){
                console.log(err);
                // res.json({ message: "error" });
            }else{
                if(resp==null){
                    console.log("The Username is incorrect" );
                    // res.json({ message: "The Username is incorrect" });
                    res.status(200).json({ message: "The Username is incorrect" });
                }else{
                    if (resp.password==password) {
                        const payload = {
                            check:  true
                        };
                        const token = jwt.sign(payload, config.llave, {
                            expiresIn:  120
                        });
                        console.log('Authenticated user');
                        res.json({
                            mensaje: 'Authenticated user',
                            token: token
                        });
                    } else {
                        console.log( "The pass is incorrect");
                        res.status(200).json({ mensaje: "The pass is incorrect" });
                        // res.json({ mensaje: "The pass is incorrect"})
                    }
                }
            }
        })
    }
    
}

module.exports = publicController;